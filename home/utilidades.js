class SellMenuCatUtilidades extends HTMLElement {
    constructor () {
      super();
    }
    
    connectedCallback () {
        this.innerHTML = `
        <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="text-left">
                <img src="./assets/img/logos/logo-imagen.png" alt="logo imagen" height="60" width="auto" class="drop">
            </div>
            <div class="row mt-3">
                <div class="col text-light bg-Flutter rot-2 l-menu"><a href="./componentes/ICON/gallery-icons.html" target="_blank"> GALERIA ICONS </a></div>
                <div class="col text-light bg-Flutter pos-z l-menu"><a href="./componentes/ANGULAR/galeria_angular.html" target="_blank"> GALERIA ANGULAR</a></div>
                <div class="col text-light bg-Flutter rot-1 l-menu"><a href="./componentes/ICON/gallery-icons.html" target="_blank"> ----</a></div>
                <div class="col text-light bg-Flutter rot-2 l-menu"><a href="#herramientas-desarrollo"> ----</a></div>
                <div class="col text-light bg-Flutter rot-1 l-menu"><a href="#diseno-estilo">------</a></div>
                <div class="col text-light bg-Flutter pos-z l-menu"><a href="#php-wordpress">-------</a></div>
                <div class="col text-light bg-Flutter rot-2 l-menu"><a href="#css">-----</a></div>
                <div class="col text-light bg-Flutter pos-z l-menu"><a href="#librerias-frameworks">-----</a></div>
                <div class="col text-light bg-Flutter pos-z l-menu"><a href="#editores-online">------</a></div>
                <div class="col text-light bg-Flutter rot-2 l-menu"><a href="#programas-recursos">-------</a></div>
                <div class="col text-light bg-Flutter pos-z l-menu"><a href="#linux">------</a></div>
                <div class="col text-light bg-Flutter pos-z l-menu"><a href="#comercio">------</a></div>
                <div class="col text-light bg-Flutter rot-1 l-menu"><a href="#libros">-----</a></div>
                <div class="col text-light bg-Flutter rot-3 l-menu"><a href="#almacenamiento">-----</a></div>
                <div class="col text-light bg-Flutter pos-z l-menu"><a href="#traductores">-----</a></div>
                <div class="col text-light bg-Flutter rot-3 l-menu"><a href="#sena">----</a></div>
                <div class="col text-light bg-Flutter pos-z l-menu"><a href="#links">------</a></div>
            </div>
        </div>
        <div class="col-md-1"></div>
        </div>
        `;
    }
}
window.customElements.define('sell-utilidades', SellMenuCatUtilidades)