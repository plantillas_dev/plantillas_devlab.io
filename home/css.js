class SellMenuCatCSS extends HTMLElement {
    constructor () {
      super();
    }
    
    connectedCallback () {
        this.innerHTML = `
        <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="text-left">
                <img src="./styles/assets/categorias/css.png" alt="logo angular" height="60" width="auto" class="drop">
            </div>
            <div class="row mt-3">
                <div class="col text-light bg-Css rot-2 l-menu"><a href="./styles/css/max.css" target="_blank"> CSS MAX </a></div>
                <div class="col text-light bg-Css pos-z l-menu"><a href="./styles/css/maxicons-v1.0/demo.html" target="_blank"> ICONMAX </a></div>
                <div class="col text-light bg-Css rot-1 l-menu"><a href="./componentes/ICON/gallery-icons.html" target="_blank"> LOGOS .PNG</a></div>
                <div class="col text-light bg-Css rot-2 l-menu"><a href="#herramientas-desarrollo">------</a></div>
                <div class="col text-light bg-Css rot-1 l-menu"><a href="#diseno-estilo">------</a></div>
                <div class="col text-light bg-Css pos-z l-menu"><a href="#php-wordpress">-------</a></div>
                <div class="col text-light bg-Css rot-2 l-menu"><a href="#css">-----</a></div>
                <div class="col text-light bg-Css pos-z l-menu"><a href="#librerias-frameworks">-----</a></div>
                <div class="col text-light bg-Css pos-z l-menu"><a href="#editores-online">------</a></div>
                <div class="col text-light bg-Css rot-2 l-menu"><a href="#programas-recursos">-------</a></div>
                <div class="col text-light bg-Css pos-z l-menu"><a href="#linux">------</a></div>
                <div class="col text-light bg-Css pos-z l-menu"><a href="#comercio">------</a></div>
                <div class="col text-light bg-Css rot-1 l-menu"><a href="#libros">-----</a></div>
                <div class="col text-light bg-Css rot-3 l-menu"><a href="#almacenamiento">-----</a></div>
                <div class="col text-light bg-Css pos-z l-menu"><a href="#traductores">-----</a></div>
                <div class="col text-light bg-Css rot-3 l-menu"><a href="#sena">----</a></div>
                <div class="col text-light bg-Css pos-z l-menu"><a href="#links">------</a></div>
            </div>
        </div>
        <div class="col-md-1"></div>
        </div>
        `;
    }
}
window.customElements.define('sell-css', SellMenuCatCSS)