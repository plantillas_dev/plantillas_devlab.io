class SellMenuCatAPIS extends HTMLElement {
    constructor () {
      super();
    }
    
    connectedCallback () {
        this.innerHTML = `
        <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="text-left">
                <img src="./styles/assets/icon/api.png" alt="logo angular" height="60" width="auto" class="drop">
            </div>
            <div class="row mt-3">
                <div class="col text-light bg-Php rot-2 l-menu"><a href="./assets/json/estadistica.json" target="_blank"> ESTADISTICA </a></div>
                <div class="col text-light bg-Php pos-z l-menu"><a href="./assets/json/frases.json" target="_blank"> FRASES </a></div>
                <div class="col text-light bg-Php rot-1 l-menu"><a href="./assets/json/productos.json" target="_blank"> PRODUCTOS D1 </a></div>
                <div class="col text-light bg-Php rot-2 l-menu"><a href="./assets/json/recursos.json" target="_blank"> TIPO DOC / PAISES </a></div>
                <div class="col text-light bg-Php rot-1 l-menu"><a href="#diseno-estilo">------</a></div>
                <div class="col text-light bg-Php pos-z l-menu"><a href="#php-wordpress">------</a></div>
                <div class="col text-light bg-Php rot-2 l-menu"><a href="#css">----</a></div>
                <div class="col text-light bg-Php pos-z l-menu"><a href="#librerias-frameworks">-----</a></div>
                <div class="col text-light bg-Php pos-z l-menu"><a href="#editores-online">-----</a></div>
                <div class="col text-light bg-Php rot-2 l-menu"><a href="#programas-recursos">------</a></div>
                <div class="col text-light bg-Php pos-z l-menu"><a href="#linux">-----</a></div>
                <div class="col text-light bg-Php pos-z l-menu"><a href="#comercio">------</a></div>
                <div class="col text-light bg-Php rot-1 l-menu"><a href="#libros">----</a></div>
                <div class="col text-light bg-Php rot-3 l-menu"><a href="#almacenamiento">------</a></div>
                <div class="col text-light bg-Php pos-z l-menu"><a href="#traductores">-----</a></div>
                <div class="col text-light bg-Php rot-3 l-menu"><a href="#sena">-----</a></div>
                <div class="col text-light bg-Php pos-z l-menu"><a href="#links">-----</a></div>
            </div>
        </div>
        <div class="col-md-1"></div>
        </div>
        `;
    }
}
window.customElements.define('sell-apis', SellMenuCatAPIS)