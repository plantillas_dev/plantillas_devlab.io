class SellIndexHome extends HTMLElement {
    constructor () {
      super();
    }
    
    connectedCallback () {
      this.innerHTML = `
      <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
          <div class="row mt-5">
              <div class="col t2 bg-light rot-2 l-menu"><a href="../../styles/css/max.css" class="t2">CSS MAX</a></div>
              <div class="col t2 bg-light pos-z l-menu"><a href="../../ANGULAR/ng-forms.html" class="t2">FORMULARIOS ANGULAR</a></div>
              <div class="col t2 bg-light rot-1 l-menu"><a href="#google-avanzados" class="t2">GOOGLE AVANZADOS</a></div>
              <div class="col t2 bg-light rot-2 l-menu"><a href="#herramientas-desarrollo" class="t2">HERRAMIENTAS DESARROLLO...</a></div>
              <div class="col t2 bg-light rot-1 l-menu"><a href="#diseno-estilo" class="t2">DISEÑO - ESTILO - WEB</a></div>
              <div class="col t2 bg-light pos-z l-menu"><a href="#php-wordpress" class="t2">PHP Y WORDPRESS</a></div>
              <div class="col t2 bg-light rot-2 l-menu"><a href="#css" class="t2">CSS</a></div>
              <div class="col t2 bg-light pos-z l-menu"><a href="#librerias-frameworks" class="t2">LIBRERÍAS Y FRAMEWORKS</a></div>
              <div class="col t2 bg-light pos-z l-menu"><a href="#editores-online" class="t2">EDITORES ONLINE...</a></div>
              <div class="col t2 bg-light rot-2 l-menu"><a href="#programas-recursos" class="t2">PROGRAMAS Y RECURSOS</a></div>
              <div class="col t2 bg-light pos-z l-menu"><a href="#linux" class="t2">LINUX</a></div>
              <div class="col t2 bg-light pos-z l-menu"><a href="#comercio" class="t2">COMERCIO</a></div>
              <div class="col t2 bg-light rot-1 l-menu"><a href="#libros" class="t2">LIBROS</a></div>
              <div class="col t2 bg-light rot-3 l-menu"><a href="#almacenamiento" class="t2">ALMACENAMIENTO</a></div>
              <div class="col t2 bg-light pos-z l-menu"><a href="#traductores" class="t2">TRADUCTORES</a></div>
              <div class="col t2 bg-light rot-3 l-menu"><a href="#sena" class="t2">SENA</a></div>
              <div class="col t2 bg-light pos-z l-menu"><a href="#links" class="t2">OTROS LINKS</a></div>
          </div>
      </div>
      <div class="col-md-1"></div>
      </div>
    `;
    }
  }
window.customElements.define('sell-index-home', SellIndexHome)