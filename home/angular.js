class SellMenuCatAngular extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
    <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="text-left">
            <img src="./styles/assets/categorias/angular.png" alt="logo angular" height="60" width="auto" class="drop">
        </div>
        <div class="row mt-3">
            <div class="col text-light bg-Angular pos-z l-menu"><a href="../../componentes/ANGULAR/ng-forms.html"> FORMULARIOS ANGULAR </a></div>
            <div class="col text-light bg-Angular rot-1 l-menu"><a href="#google-avanzados">------</a></div>
            <div class="col text-light bg-Angular rot-2 l-menu"><a href="#herramientas-desarrollo">-------</a></div>
            <div class="col text-light bg-Angular rot-1 l-menu"><a href="#diseno-estilo">--------</a></div>
            <div class="col text-light bg-Angular pos-z l-menu"><a href="#php-wordpress">---------</a></div>
            <div class="col text-light bg-Angular rot-2 l-menu"><a href="#css">-----</a></div>
            <div class="col text-light bg-Angular pos-z l-menu"><a href="#librerias-frameworks">------</a></div>
            <div class="col text-light bg-Angular pos-z l-menu"><a href="#editores-online">------</a></div>
            <div class="col text-light bg-Angular rot-2 l-menu"><a href="#programas-recursos">-------</a></div>
        </div>
    </div>
    <div class="col-md-1"></div>
    </div>
    `;
    }
}
window.customElements.define('sell-angular', SellMenuCatAngular)