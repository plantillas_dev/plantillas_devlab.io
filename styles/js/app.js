jQuery('#ejemplo2').typeIt({
    strings: ["Esto es una línea de texto", "Se puede escribir en más de una línea."],
    speed: 50
});


let datosPersona = [
    {
        nombres: 'Ana Alejandra',
        apellidos: 'Gaviria',
        telefono: 3148007248
    },
    {
        nombres: 'Jorge Andres',
        apellidos: 'Lopera',
        telefono: 3141234255
    },
    {
        nombres: 'Sandra Maria',
        apellidos: 'Lozano',
        telefono: 3162346447
    }
];

let listaNombres = datosPersona.map( ( item ) => {
    return item.nombres;
});

console.log(listaNombres); // [Ana Alejandra, Jorge Andres, Sandra Maria]